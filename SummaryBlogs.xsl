<?xml version="1.0" encoding="UTF-8"?>
<!--
    Document   : SummaryBlogs.xsl
    Created on : January 3, 2013, 1:26 AM
    Author     : admin
    Description:
        Purpose of transformation follows.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/items">
        <html>
            <body>
                <h1>Recent Blog Entries at http://gotoknow.org</h1>
                <table border="1" table-layout="fixed" width="100%">
                    <tr bgcolor="lightgreen">
                        <th align="left" width="40%">Title</th>
                        <th align="left" width="30%">Author</th>
                        <th align="left" width="30%">Link</th>
                    </tr>                
                    <xsl:for-each select="item">
                        <tr>
                            <td>
                                <xsl:value-of select="title"/>
                            </td>
                            <td>
                                <xsl:value-of select="author"/>
                            </td>
                            <td>
                                <a>
                                    <xsl:attribute name="href"> 
                                        <xsl:value-of select="link"/> 
                                    </xsl:attribute>
                                    <xsl:value-of select="link"/>
                                </a>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>        
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
