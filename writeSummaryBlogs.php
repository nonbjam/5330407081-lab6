<?php 
		$dom = new DomDocument('1.0','UTF-8'); 
		$feed = file_get_contents("http://www.gotoknow.org/blogs/posts?format=rss");
		$xml = new SimpleXmlElement($feed);

        echo "Reading from summaryBlogs.xml...<br>";
        $items = $dom->appendChild($dom->createElement('items'));

        foreach ($xml->channel->item as $item) {
                $titleOut = $item->title;
                $linkOut = $item->link;
                $authorOut = $item->author;
                
                $item = $items->appendChild($dom->createElement('item')); 

                $title = $item->appendChild($dom->createElement('title')); 
                $title->appendChild( $dom->createTextNode($titleOut));
                $link = $item->appendChild($dom->createElement('link'));
                $link->appendChild( $dom->createTextNode($linkOut));
                $author = $item->appendChild($dom->createElement('author')); 
                $author->appendChild( $dom->createTextNode($authorOut));

                echo $linkOut . $authorOut;
        }
        $dom->formatOutput = true;
        $test1 = $dom->saveXML();
        $dom->save('summaryBlogs.xml'); 
?>